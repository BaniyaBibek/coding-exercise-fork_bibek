﻿using System;
using IPP.Recruitment.Domain.BusinessRules;
using IPP.Recruitment.Domain.DomainEntity;
using NUnit.Framework;

namespace IPP.Recruitment.Domain.UnitTests
{
    [TestFixture]
    public class CreditCardUnitTests
    {

        [TestCase]
        public void CardValidate_WithAmountLessthan99cent_ReturnsInvalid()
        {
            //Arrange
            CreditCard card = new CreditCard
            {
                Amount = 0.80m,
                CardNumber = "4111111111111111",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Now.AddYears(1).Year
            };

            //Act
            bool actualResult = card.Validate();

            //Assert
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void CardValidate_WithAmountGreaterthan99999999cent_ReturnsInvalid()
        {
            //Arrange
            CreditCard card = new CreditCard
            {
                Amount = 999999.999m,
                CardNumber = "4111111111111111",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Now.AddYears(1).Year
            };

            //Act
            bool actualResult = card.Validate();

            //Assert
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void CardValidate_WithAmountBetween99And99999999cent_ReturnsValid()
        {
            //Arrange
            CreditCard card = new CreditCard
            {
                Amount = 2m,
                CardNumber = "4111111111111111",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Now.AddYears(1).Year
            };
            //Act
            bool actualResult = card.Validate();
            //Assert
            Assert.AreEqual(true, actualResult);
        }
        [TestCase]
        public void CardValidate_WithSixteenDigitInValidCCNumber_ReturnsInvalid()
        {
            //Arrange
            CreditCard card = new CreditCard
            {
                Amount = 2m,
                CardNumber = "5555555555554443",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Now.AddYears(1).Year
            };
            //Act
            bool actualResult = card.Validate();
            //Assert
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void CardValidate_WithValidCCNumbers_ReturnsValid()
        {
            CreditCard card = new CreditCard
            {
                Amount = 2m,
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Now.AddYears(1).Year
            };
            string[] validCcNumbers = { "5555555555554444", "5105105105105100", "4111111111111111" };
            foreach (string validCcNumber in validCcNumbers)
            {
                card.CardNumber = validCcNumber;
                //Act
                bool actualResult = card.Validate();
                //Assert
                Assert.AreEqual(true, actualResult);
            }
        }
        [TestCase]
        public void CardValidate_WithFourDigitFutureYear_ReturnsValid()
        {
            CreditCard card = new CreditCard
            {
                CardNumber = "5105105105105100",
                Amount = 2m,
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Now.AddYears(1).Year
            };

            //Act
            bool actualResult = card.Validate();
            //Assert
            Assert.AreEqual(true, actualResult);

        }

        [TestCase]
        public void Validate_WithMonthBetweenOneToTwelve_ReturnsValid()
        {
            CreditCard card = new CreditCard
            {
                CardNumber = "5105105105105100",
                Amount = 2m,
                ExpiryYear = DateTime.Now.AddYears(1).Year
            };

            for (int i = 1; i <= 12; i++)
            {
                card.ExpiryMonth = i;
                //Act
                bool actualResult = card.Validate();
                //Assert
                Assert.AreEqual(true, actualResult);
            }
        }
    }

}
