﻿using IPP.Recruitment.Domain.BusinessRules;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPP.Recruitment.Domain.UnitTests
{
    [TestFixture]
    public class IsCreditCardValidUnitTests
    {
        [TestCase]
        public void Validate_WithCCdigitsLessThan16_ReturnsInvalid()
        {
            string fifteenDigitCardNumber = "411111111111111";
            var isCardNumberValid = new IsCardNumberValid(fifteenDigitCardNumber);
            bool actualResult = isCardNumberValid.Validate();
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void Validate_WithCCdigitsMoreThan16_ReturnsInvalid()
        {
            string seventeenDigitCardNumber = "41111111111111111";
            var isCardNumberValid = new IsCardNumberValid(seventeenDigitCardNumber);
            bool actualResult = isCardNumberValid.Validate();
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void Validate_WithNonNumericCCNumber_ReturnsInvalid()
        {
            string nonNumericCardNumber = "41111111a1111111";
            var isCardNumberValid = new IsCardNumberValid(nonNumericCardNumber);
            bool actualResult = isCardNumberValid.Validate();
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void Validate_WithValidCCNumbers_ReturnsValid()
        {
            string[] validCcNumbers = { "5555555555554444", "5105105105105100", "4111111111111111" };
            foreach (string validCcNumber in validCcNumbers)
            {
                var isCardNumberValid = new IsCardNumberValid(validCcNumber);
                bool actualResult = isCardNumberValid.Validate();
                Assert.AreEqual(true, actualResult);
            }
        }
        [TestCase]
        public void Validate_WithSixteenDigitInValidCCNumber_ReturnsInvalid()
        {
            string SixteenDigitInValidCCNumber = "5555555555554443";
            var isCardNumberValid = new IsCardNumberValid(SixteenDigitInValidCCNumber);
            bool actualResult = isCardNumberValid.Validate();
            Assert.AreEqual(false, actualResult);
        }
    }
}
