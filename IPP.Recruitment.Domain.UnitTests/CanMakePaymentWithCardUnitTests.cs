﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPP.Recruitment.Domain.BusinessRules;
using NUnit.Framework;

namespace IPP.Recruitment.Domain.UnitTests
{
    [TestFixture]
    public class CanMakePaymentWithCardUnitTests
    {
        [TestCase]
        public void Validate_WithCCdigitsLessThan16_ReturnsInvalid()
        {
            string fifteenDigitCardNumber = "411111111111111";
            var canMakePaymentWithCard = new CanMakePaymentWithCard(fifteenDigitCardNumber, 12, DateTime.Now.AddYears(1).Year);
            bool actualResult = canMakePaymentWithCard.Validate();
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void Validate_WithCCdigitsMoreThan16_ReturnsInvalid()
        {
            string seventeenDigitCardNumber = "41111111111111111";
            var canMakePaymentWithCard = new CanMakePaymentWithCard(seventeenDigitCardNumber, 12, DateTime.Now.AddYears(1).Year);
            bool actualResult = canMakePaymentWithCard.Validate();
            Assert.AreEqual(false, actualResult);
        }

        [TestCase]
        public void Validate_WithMonthLessThanOne_ReturnsInvalid()
        {
            string seventeenDigitCardNumber = "4111111111111111";
            var canMakePaymentWithCard = new CanMakePaymentWithCard(seventeenDigitCardNumber, 0, DateTime.Now.AddYears(1).Year);
            bool actualResult = canMakePaymentWithCard.Validate();
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void Validate_WithMonthGreaterThan12_ReturnsInvalid()
        {
            string seventeenDigitCardNumber = "4111111111111111";
            var canMakePaymentWithCard = new CanMakePaymentWithCard(seventeenDigitCardNumber, 13, DateTime.Now.AddYears(1).Year);
            bool actualResult = canMakePaymentWithCard.Validate();
            Assert.AreEqual(false, actualResult);
        }
        [TestCase]
        public void Validate_WithMonthBetweenOneToTwelve_ReturnsValid()
        {
            string seventeenDigitCardNumber = "4111111111111111";
            for (int i = 1; i <= 12; i++)
            {

                var canMakePaymentWithCard = new CanMakePaymentWithCard(seventeenDigitCardNumber, i,
                    DateTime.Now.AddYears(1).Year);
                bool actualResult = canMakePaymentWithCard.Validate();
                Assert.AreEqual(true, actualResult);
            }
        }
        [TestCase]
        public void Validate_WithYearLessThanFourDigits_ReturnsInValid()
        {
            string seventeenDigitCardNumber = "4111111111111111";
            var canMakePaymentWithCard = new CanMakePaymentWithCard(seventeenDigitCardNumber, 2,
                     22222);
            bool actualResult = canMakePaymentWithCard.Validate();
            Assert.AreEqual(false, actualResult);

        }
        [TestCase]
        public void Validate_WithFourDigitFutureYear_ReturnsValid()
        {
            string seventeenDigitCardNumber = "4111111111111111";
            int currentYrPlusOne = DateTime.Now.AddYears(1).Year;
            var canMakePaymentWithCard = new CanMakePaymentWithCard(seventeenDigitCardNumber, 2,
                     currentYrPlusOne);
            bool actualResult = canMakePaymentWithCard.Validate();
            Assert.AreEqual(true, actualResult);

        }
        [TestCase]
        public void Validate_WithFourDigitPastYear_ReturnsInValid()
        {
            string seventeenDigitCardNumber = "4111111111111111";
            int currentYrMinusOne = DateTime.Now.AddYears(-1).Year;
            var canMakePaymentWithCard = new CanMakePaymentWithCard(seventeenDigitCardNumber, 2,
                     currentYrMinusOne);
            bool actualResult = canMakePaymentWithCard.Validate();
            Assert.AreEqual(false, actualResult);

        }
        [TestCase]
        public void Validate_WithFourDigitNegativeYear_ReturnsInValid()
        {
            string seventeenDigitCardNumber = "4111111111111111";
            int currentYrPlusOne = -(DateTime.Now.AddYears(1).Year);
            var canMakePaymentWithCard = new CanMakePaymentWithCard(seventeenDigitCardNumber, 2,
                     currentYrPlusOne);
            bool actualResult = canMakePaymentWithCard.Validate();
            Assert.AreEqual(false, actualResult);

        }
    }
}
