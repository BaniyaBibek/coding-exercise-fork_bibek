﻿using IPP.Recruitment.Domain.BusinessRules;

namespace IPP.Recruitment.Domain.DomainEntity
{
    public class CreditCard : BaseEntity
    {

        /// <summary>
        /// A 16 digit card number
        /// </summary>
        public string CardNumber { get; set; }
        /// <summary>
		/// Represents payment amount between 99 cents and 99999999 cents
		/// </summary>
        public decimal Amount { get; set; }
        /// <summary>
		/// Credit expiry month 1 or 2 digits 
		/// </summary>
        public int ExpiryMonth { get; set; }
        /// <summary>
		/// Credit expiry year 4 digits
		/// </summary>
        public int ExpiryYear { get; set; }

        public bool Validate()
        {
            AddBusinessruleValidators();
            bool isValid = true;

            foreach (var rule in BusinessRules)
            {
                if (!rule.Validate())
                {
                    isValid = false;
                }
            }
            return isValid;
        }
        private void AddBusinessruleValidators()
        {
            AddBusinessRule(new IsCardNumberValid(CardNumber));
            AddBusinessRule(new CanMakePaymentWithCard(CardNumber, ExpiryMonth, ExpiryYear));
            AddBusinessRule(new IsValidPaymentAmount(Amount));
        }

    }
}