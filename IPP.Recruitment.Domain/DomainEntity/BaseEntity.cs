﻿using IPP.Recruitment.Domain.BusinessRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPP.Recruitment.Domain.DomainEntity
{
    public abstract class BaseEntity
    {
        protected BaseEntity()
        {
            BusinessRules.Clear();
        }
        protected IList<IBusinessRule> BusinessRules = new List<IBusinessRule>();

        protected void AddBusinessRule(IBusinessRule businessRule)
        {
            BusinessRules.Add(businessRule);
        }

    }
}
