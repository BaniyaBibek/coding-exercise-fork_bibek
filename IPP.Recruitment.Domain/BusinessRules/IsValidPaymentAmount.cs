﻿namespace IPP.Recruitment.Domain.BusinessRules
{
    public class IsValidPaymentAmount : IBusinessRule
    {
        private readonly decimal _amount;
        /// <summary>
		/// Checks if the amount represents a valid payment amount 
		/// </summary>
		/// <param name="amount">An amount value in cents (1 Dollar = 100 cents)</param>
		/// <remarks>
		/// Validation:
		/// The amount must be between 99 cents and 99999999 cents
		/// </remarks>
        public IsValidPaymentAmount(decimal amount)
        {
            _amount = amount;
        }
        public bool Validate()
        {
            if (_amount < 0.99m)
            {
                return false;
            }
            if (_amount > 999999.99m)
            {
                return false;
            }
            return true;
        }
    }
}