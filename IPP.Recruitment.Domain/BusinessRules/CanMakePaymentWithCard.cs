﻿using System;
using System.Text.RegularExpressions;

namespace IPP.Recruitment.Domain.BusinessRules
{
    public class CanMakePaymentWithCard : IBusinessRule
    {
        private readonly string _cardNumber;
        private readonly int _expiryMonth;
        private readonly int _expiryYear;
        /// <summary>
		/// Validates the card number, expiry motnh and year to ensure the details can be used to make a payment
		/// </summary>
		/// <param name="cardNumber">A 16 digit card number</param>
		/// <param name="expiryMonth">Month part of the expiry date</param>
		/// <param name="expiryYear">Year part of the expiry date</param>
		/// <returns>true if the details represent a valid card, otherwise false</returns>
		/// <remarks>
		/// Validations:
		/// cardNumber: Ensure the passed string is 16 in length and passes the MOD10/LUHN check
		/// expiryMonth: should represent a month number between 1 and 12
		/// expiryYear: Should represent a year value, 4 characters in lenght and either the current or a future year
		/// The expiry month + year should represent a date in the future
		/// </remarks>
        public CanMakePaymentWithCard(string cardNumber, int expiryMonth, int expiryYear)
        {
            _cardNumber = cardNumber;
            _expiryMonth = expiryMonth;
            _expiryYear = expiryYear;
        }
        public bool Validate()
        {
            return _cardNumber.Length == 16 && (ExpiryMonthisValid() && ExpiryYearIsValid());
        }

        private bool ExpiryYearIsValid()
        {
            if (!YearIsFourDigit())
            {
                return false;
            }
            int currentYear = DateTime.Now.Year;
            if (_expiryYear < currentYear)
            {
                return false;
            }
            return true;
        }

        private bool YearIsFourDigit()
        {
            return Regex.IsMatch(_expiryYear.ToString(), @"^\d{4}$", RegexOptions.IgnoreCase);
        }

        private bool ExpiryMonthisValid()
        {
            return _expiryMonth >= 1 && _expiryMonth <= 12;
        }
    }
}