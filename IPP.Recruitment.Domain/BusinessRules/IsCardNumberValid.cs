﻿using System;

namespace IPP.Recruitment.Domain.BusinessRules
{
    /// <summary>
    /// Performs a Mod-10/LUHN check on the passed number and returns true if the check passed
    /// </summary>
    /// <param name="cardNumber">A 16 digit card number</param>
    /// <returns>true if the card number is valid, otherwise false</returns>
    /// <remarks>
    /// Refer here for MOD10 algorithm: https://en.wikipedia.org/wiki/Luhn_algorithm
    /// </remarks>
    public class IsCardNumberValid : IBusinessRule
    {
        readonly string _cardNumber;
        public IsCardNumberValid(string cardNumber)
        {
            _cardNumber = cardNumber;
        }
        public bool Validate()
        {
            const int creditCardLength = 16;

            if (!IsSixteenDigitCreditCard(creditCardLength)) return false;

            var creditCardDigits = GetAllDigitsInCreditCard(creditCardLength);


            int sumOfDigits = 0;
            bool isAlternateDigit = false;
            for (var digitPosition = creditCardLength - 1; digitPosition >= 0; digitPosition--)
            {

                int currentRightmostDigit = creditCardDigits[digitPosition];
                if (isAlternateDigit)
                {
                    currentRightmostDigit = DoubleIt(currentRightmostDigit);
                }
                sumOfDigits += currentRightmostDigit;
                isAlternateDigit = !isAlternateDigit;
            }
            if (!Modulo10EqualZero(sumOfDigits)) return false;

            return true;
        }

        private static bool Modulo10EqualZero(int sumOfDigits)
        {
            if (sumOfDigits % 10 != 0)
            {
                return false;
            }
            return true;
        }

        private static int DoubleIt(int currentRightmostDigit)
        {
            currentRightmostDigit *= 2;
            currentRightmostDigit = SubtractNineIfGreaterThanNine(currentRightmostDigit);
            return currentRightmostDigit;
        }

        private static int SubtractNineIfGreaterThanNine(int currentRightmostDigit)
        {
            if (currentRightmostDigit > 9)
            {
                currentRightmostDigit -= 9;
            }
            return currentRightmostDigit;
        }

        private int[] GetAllDigitsInCreditCard(int creditCardLength)
        {
            int[] creditCardDigits = new int[creditCardLength];
            for (int digitPosition = 0; digitPosition < creditCardLength; digitPosition++)
            {
                creditCardDigits[digitPosition] = int.Parse(_cardNumber.Substring(digitPosition, 1));
            }
            return creditCardDigits;
        }

        private bool IsSixteenDigitCreditCard(int creditCardLength)
        {
            if (string.IsNullOrEmpty(_cardNumber))
            {
                return false;
            }

            if (_cardNumber.Length != creditCardLength)
            {
                return false;
            }
            long cardNumber;
            long.TryParse(_cardNumber, out cardNumber);
            if (cardNumber == 0)
            {
                return false;
            }
            return true;
        }
    }
}