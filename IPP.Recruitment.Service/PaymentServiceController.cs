﻿using System;
using System.Web.Http;
using IPP.Recruitment.Domain;
using IPP.Recruitment.Domain.DomainEntity;

namespace IPP.Recruitment.Service
{
    public class PaymentServiceController : ApiController, IPaymentService
    {
        [HttpPost]
        public Guid MakePayment(CreditCard creditCard)
        {

            bool isValid = creditCard.Validate();
            if (isValid)
            {
                return Guid.NewGuid();
            }

            return Guid.Empty;

        }


        [HttpGet]
        public string WhatsYourId()
        {
            return "008ab27c-36b2-43e5-91d5-edbd1e5b564b";
        }
    }
}
